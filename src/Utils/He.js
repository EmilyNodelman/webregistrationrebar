const he = 
{
    advertisingTerms: "אני מסכים/ה שתשלחו אלי חומר  פרסומי ושיווקי של rebar בכל אמצעי התקשורת ובהתאם למדיניות הפרטיות",
    registrationTerms: "אני מאשר/ת את הצטרפתי למועדון הלקוחות ושקראתי את תקנון מועדון הלקוחות ומסכימ/ה לכל תנאיו",
    title:"הצטרפות למועדון",
    privateName:"שם פרטי",
    familyName: "שם משפחה",
    phone: "טלפון",
    birthdate: "מתי יש לך יום הולדת?",
    update: "עדכן"
}

export default he;

