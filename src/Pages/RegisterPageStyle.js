import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
  root: {},
  header: {
    textAlign: 'center',
  },
  checkbox: {
    marginTop: '15px',
    justifyContent: 'flex-end',
    textAlign: 'left',
  },
  termsText: {
    marginLeft: '2.34%',
  },
  updateButton: {
    marginTop: '20%',
    border: '0.5px solid #CECECE',
    opacity: '90%',
    backgroundColor: '#6CA93F',
    borderRadius: '25px',
    height: '40.5px',
    width: '200.5px',
    boxShadow: ' 0 -3px 6px 0 rgba(0,0,0,0.11)',
  },
  pineappleImg: {
    position: 'absolute',
    top: '0',
    right: '-66px',
    marginTop: '10px',
  },
  blueberryImg: {
    left: '-30px',
    position: 'absolute',
    top: '290px',
  },
  form: {
    position: 'relative',
  },
  joinClub: {
    fontSize: '26px',
    marginBottom: '20px',
    marginTop: '30px',
    fontWeight: 'bold',
  },
  update: {
    marginTop: '5px',
  },
  personalInfoSpaces: {
    paddingTop: '20px',
  },
  controlersContainer: {
    marginRight: 50,
    marginLeft: 50,
  },
  inputStyle: {
    fontWeight: 'bold',
  },
})
export default useStyles
