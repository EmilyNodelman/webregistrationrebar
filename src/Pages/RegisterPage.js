import React, { useState } from 'react'
import TextField from '@material-ui/core/TextField'
import { useForm, Controller } from 'react-hook-form'
import useStyles from './RegisterPageStyle'
import he from '../Utils/He'
import checkbox from '../Assets/Images/checkbox/Checkbox - Normal.svg'
import errorCheckbox from '../Assets/Images/checkbox/Checkbox - error.svg'
import checkboxChecked from '../Assets/Images/checkbox/Checkbox - full.svg'
import pineapple from '../Assets/Images/pinapple/pineapple.png'
import blueberry from '../Assets/Images/blueberry/blueberry.png'
import updateButton from '../Assets/Images/updateButton/update.png'
import { withStyles } from '@material-ui/core/styles'

const CssTextField = withStyles({
  root: {
    display: 'flex',
    lineHeight: '21px',
    opacity: '0.5',
    '& label.Mui-focused': {
      color: '#95A8AC',
    },
    '& .MuiInputBase-root': {
      color: 'black',
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: '#95A8AC',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: '#95A8AC',
      },
      '&:hover fieldset': {
        borderColor: '#95A8AC',
      },
      '&.Mui-focused fieldset': {
        borderColor: '#95A8AC',
      },
    },
  },
})(TextField)

const CssTextFieldError = withStyles({
  root: {
    display: 'flex',
    lineHeight: '2px',
    opacity: '0.5',
    color: '#95A8AC',
    '& label': {
      color: '#95A8AC !important',
    },
    '& .MuiInputBase-root': {
      color: 'black',
      fontSize: '28',
    },
    '& label.Mui-focused': {
      color: '#95A8AC',
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: 'red',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: '#95A8AC',
      },
      '&:hover fieldset': {
        borderColor: '#95A8AC',
      },
      '&.Mui-focused fieldset': {
        borderColor: '#95A8AC',
      },
    },
  },
})(TextField)

const RegisterClub = () => {
  const {
    control,
    formState: { errors },
  } = useForm()
  const classes = useStyles()
  const [adverCheckBoxImg, setAdverCheckBoxImg] = useState(checkbox)
  const [confirmCheckBoxImg, setConfirmCheckBoxImg] = useState(checkbox)

  const [name, setTextName] = useState('')
  const [nameError, setTextnameError] = useState(false)
  const [family, setTextfamily] = useState('')
  const [familyError, setTextfamilyError] = useState(false)
  const [birthDate, setTextbirthDate] = useState('')
  const [birthDateError, setTextbirthDateError] = useState(false)
  const [phone, setTextphone] = useState('')
  const [phoneError, setTextphoneError] = useState(false)

  const handleSubmit = (e) => {
    e.preventDefault()

    setTextnameError(false)
    setTextfamilyError(false)
    setTextphoneError(false)
    setTextbirthDateError(false)

    if (name == '') {
      setTextnameError(true)
    }

    if (family == '') {
      setTextfamilyError(true)
    }

    if (phone == '' || phone.length != 10) {
      setTextphoneError(true)
    }

    if (birthDate == '') {
      setTextbirthDateError(true)
    }

    if (adverCheckBoxImg == checkbox) {
      setAdverCheckBoxImg(errorCheckbox)
    }

    if (confirmCheckBoxImg == checkbox) {
      setConfirmCheckBoxImg(errorCheckbox)
    }
  }

  const adverCheckBoxClick = () => checkBoxClick(adverCheckBoxImg, setAdverCheckBoxImg)
  const confirmCheckBoxClick = () => checkBoxClick(confirmCheckBoxImg, setConfirmCheckBoxImg)

  const checkBoxClick = (src, set) => {
    if (src === checkbox) {
      set(checkboxChecked)
    } else {
      set(checkbox)
    }
  }

  return (
    <>
      <div className={classes.joinClub}> {he.title}</div>
      <form autoComplete="off" className={classes.form} onSubmit={handleSubmit}>
        <div className={classes.pineappleImg}>
          <img src={pineapple} width="134.66px" height="183.59px" />
        </div>

        <div className={classes.blueberryImg}>
          <img src={blueberry} width="79px" height="72px" />
        </div>

        <div className={classes.controlersContainer}>
          <Controller
            name="firstName"
            control={control}
            render={({ field }) =>
              nameError ? (
                <CssTextFieldError
                  className={classes.inputStyle}
                  {...field}
                  value={name}
                  error={nameError}
                  label={he.privateName}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  onChange={(e) => setTextName(e.target.value)}
                />
              ) : (
                <CssTextField
                  {...field}
                  value={name}
                  error={nameError}
                  label={he.privateName}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  onChange={(e) => setTextName(e.target.value)}
                />
              )
            }
          />
          <div className={classes.personalInfoSpaces} />

          <Controller
            name="lastName"
            control={control}
            render={({ field }) =>
              familyError ? (
                <CssTextFieldError
                  {...field}
                  value={family}
                  error={familyError}
                  label={he.familyName}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  onChange={(e) => setTextfamily(e.target.value)}
                />
              ) : (
                <CssTextField
                  {...field}
                  value={family}
                  error={familyError}
                  onChange={(e) => setTextfamily(e.target.value)}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  label={he.familyName}
                />
              )
            }
          />
          <div className={classes.personalInfoSpaces} />

          <Controller
            name="phone"
            control={control}
            render={({ field }) =>
              phoneError ? (
                <CssTextFieldError
                  {...field}
                  value={phone}
                  type="number"
                  error={phoneError}
                  label={he.phone}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  onChange={(e) => setTextphone(e.target.value)}
                />
              ) : (
                <CssTextField
                  {...field}
                  value={phone}
                  type="number"
                  error={phoneError}
                  onChange={(e) => setTextphone(e.target.value)}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  label={he.phone}
                />
              )
            }
          />
          <div className={classes.personalInfoSpaces} />

          <Controller
            name="birthDate"
            control={control}
            render={({ field }) =>
              birthDateError ? (
                <CssTextFieldError
                  {...field}
                  value={birthDate}
                  error={birthDateError}
                  label={he.birthdate}
                  type="date"
                  InputLabelProps={{
                    shrink: true,
                  }}
                  onChange={(e) => setTextbirthDate(e.target.value)}
                />
              ) : (
                <CssTextField
                  {...field}
                  value={birthDate}
                  error={birthDateError}
                  onChange={(e) => setTextbirthDate(e.target.value)}
                  type="date"
                  InputLabelProps={{
                    shrink: true,
                  }}
                  label={he.birthdate}
                />
              )
            }
          />

          <div className={classes.personalInfoSpaces} />

          <div className={classes.checkbox} onClick={adverCheckBoxClick}>
            <img src={adverCheckBoxImg} width="18" height="18" />
            <label className={classes.termsText}>{he.advertisingTerms}</label>
          </div>

          <div className={classes.checkbox} onClick={confirmCheckBoxClick}>
            <img src={confirmCheckBoxImg} width="18" height="18" />
            <label className={classes.termsText}>{he.registrationTerms}</label>
          </div>

          <div>
            <button type="submit" className={classes.updateButton}>
              <img className={classes.update} src={updateButton} width="32px" height="16px" />
            </button>
          </div>
        </div>
      </form>
    </>
  )
}
export default RegisterClub
