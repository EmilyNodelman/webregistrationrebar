import React from 'react'
import { create } from 'jss'
import rtl from 'jss-rtl'
import { StylesProvider, jssPreset, ThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline'
import './App.css'
import RegisterClub from './Pages/RegisterPage'
import { Typography } from '@material-ui/core'

const jss = create({ plugins: [...jssPreset().plugins, rtl()] })

const rtlTheme = createMuiTheme({ direction: 'rtl' })

function App() {
  return (
    <StylesProvider jss={jss}>
      <ThemeProvider theme={rtlTheme}>
        <CssBaseline />
        <div className="App">
          <RegisterClub />
        </div>
      </ThemeProvider>
    </StylesProvider>
  )
}

export default App
